import os
from flask import Flask
from flask import jsonify
from flask import request

app = Flask(__name__, static_url_path='/static')

@app.route("/")
def hello():
	#url_for('static', filename='jeison.json')
	return app.send_static_file('jeison.json')

if __name__ == "__main__":
    port = int(os.environ.get("PORT", 5000))
    app.run(host='0.0.0.0', port=port)
