package com.example.luis.hackdefinitive;

/**
 * Created by Luis on 04/03/2017.
 */

class Coordenada{
    int x, y;
    Coordenada(int x, int y){
        this.x = x;
        this.y = y;
    }
}

public class Producto {
    String nombre;
    Coordenada coord;

    Producto(String nombre, Coordenada coord){
        this.nombre = nombre;
        this.coord = coord;
    }
}
