package com.example.luis.hackdefinitive;

import java.util.ArrayList;

/**
 * Created by Luis on 04/03/2017.
 */

public class Supermercado {

    ArrayList<ArrayList<Boolean>> matrizSupermercado;

    Supermercado(){
        this.matrizSupermercado = new ArrayList<ArrayList<Boolean>>();
        for (int i = 0; i < 40; i++){
            matrizSupermercado.add(new ArrayList<Boolean>());
            for (int j = 0; j < 40; j++){
                matrizSupermercado.get(i).add(false);
            }
        }
        //printarMatriz();
    }

    void addProducto(Producto p){
        this.matrizSupermercado.get(p.coord.x).set(p.coord.y, true);
    }


    void printarMatriz(){
        for (int i = 0; i < this.matrizSupermercado.size(); i++){
            for (int j = 0; j < this.matrizSupermercado.get(i).size(); j++){
                System.out.print((Boolean)this.matrizSupermercado.get(i).get(j));
            }
            System.out.print("\n");
        }
    }
}
