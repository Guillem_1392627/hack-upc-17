package com.example.luis.hackdefinitive;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TabHost;
import android.widget.TextView;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    TabHost TbH;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TbH = (TabHost) findViewById(R.id.tabHost); //llamamos al Tabhost
        TbH.setup();                                                         //lo activamos

        TabHost.TabSpec tab1 = TbH.newTabSpec("tab1");  //aspectos de cada Tab (pestaña)
        TabHost.TabSpec tab2 = TbH.newTabSpec("tab2");
        TabHost.TabSpec tab3 = TbH.newTabSpec("tab3");

        tab1.setIndicator("MAP");    //qué queremos que aparezca en las pestañas
        tab1.setContent(R.id.rel_layout); //definimos el id de cada Tab (pestaña)

        tab2.setIndicator("PRODUCTS");
        tab2.setContent(R.id.tab2);
        LinearLayout lView = (LinearLayout) findViewById(R.id.pestana_productos);
        TextView texto = new TextView(this);
        texto.setText("Ham \nChicken \nBeef \nEggs \nMilk \nFish \nCereals \nFlour \nCookies \nJam \nWine \nBread \nNoodles \nPrawns \nApple \nLettuce \nOnion \nFlowers");
        lView.addView(texto);

        tab3.setIndicator("CART");
        tab3.setContent(R.id.tab3);
        LinearLayout lView2 = (LinearLayout) findViewById(R.id.pestana_carro);
        TextView tixt = new TextView(this);
        tixt.setText("Your shopping cart is empty by now");
        lView2.addView(tixt);

        TbH.addTab(tab1); //añadimos los tabs ya programados
        TbH.addTab(tab2);
        TbH.addTab(tab3);

        RelativeLayout rl = (RelativeLayout) findViewById(R.id.rel_layout);
        ImageView img = (ImageView) findViewById(R.id.imageView4);
        Supermercado superm = new Supermercado();
        try {
            HttpHandler sh = new HttpHandler();
            String url = "https://rdbzlabs-hackupc.scalingo.io/";
            String jsonStr = sh.makeServiceCall(url);
            JSONArray mainNode = new JSONArray(jsonStr);
            //JSONArray mainNode = reader.getJSONArray("productos");
            for (int i = 0; i < mainNode.length(); i++) {
                JSONObject jo = mainNode.getJSONObject(i);
                Coordenada c = new Coordenada(jo.getInt("coordx"), jo.getInt("coordy"));
                Producto p = new Producto(jo.getString("nombre"), c);
                superm.addProducto(p);

                LinearLayout layout = new LinearLayout(this);
                layout.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.FILL_PARENT, LinearLayout.LayoutParams.FILL_PARENT));

                int w = img.getDrawable().getIntrinsicWidth();
                int h = img.getDrawable().getIntrinsicHeight();
                RelativeLayout.LayoutParams lparams = new RelativeLayout.LayoutParams(h / 80, w / 80);

                lparams.leftMargin = 25 + c.x * h / 87;
                lparams.topMargin = 200 + c.y * w / 97;
                ImageView oImageView = new ImageView(this);
                oImageView.setImageResource(R.drawable.x);

                oImageView.setLayoutParams(lparams);
                rl.addView(oImageView);

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    public String loadJSONFromAsset() {
        StringBuilder stringBuilder = new StringBuilder();
        try {
            InputStream is = getAssets().open("productos.json");
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(is));

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
            }

            bufferedReader.close();
            return stringBuilder.toString();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.tab2) {
            Intent intent = new Intent(this, tab2.class);
            startActivity(intent);
        }
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("Main Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    @Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }
}
